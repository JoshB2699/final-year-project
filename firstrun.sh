PATH=$PATH:./bin #Add the local bin file to the users PATH.
sudo chown -hR $USER $HOME/.minikube #chown the .minikube directory to ensure access to it as a user level.
sudo chown -hR $USER $HOME/.kube #chown the .kube directory to ensure access to it as a user level.
minikube start --driver=docker #Start the minikube cluster.
eval $(minikube docker-env) #Makes docker use the internal minikube docker daemon to ensure the built container can be seen when deploying.
docker build -t calc-solver-web:1.0 ./vue #Builds the container for the app.
