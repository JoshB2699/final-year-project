# Instructions

## First run.

### Creates the required kubernetes cluster and builds the app image.
This will require sudo privileges ONLY so it can run chown, feel free to review firstrun.sh to confirm this.

`./firstrun.sh`

## Deploying app.
### Builds the app, loads it to the cluster and applies the deployment. After running this command the ip which the web app can be found at will be printed to the terminal.

`./deploy.sh`

## Cleaning up
### Stops and removes running clusters.
`./cleanup.sh`
