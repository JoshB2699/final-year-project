PATH=$PATH:./bin #Add the local bin file to the users PATH.
kubectl apply -f ./config/deployment.yaml #Applies the deployment from the config file.
kubectl expose deployment calc-solver-web --type=NodePort --port=8080 #Exposes internal port 8080.
minikube service calc-solver-web --url #Prints the url of the app to the terminal.
